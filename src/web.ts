import { WebPlugin } from '@capacitor/core';
import { BillpocketPluginPlugin } from './definitions';

export class BillpocketPluginWeb extends WebPlugin implements BillpocketPluginPlugin {
  constructor() {
    super({
      name: 'BillpocketPlugin',
      platforms: ['web']
    });
  }

  async echo(options: { value: string }): Promise<{value: string}> {
    console.log('ECHO', options);
    return options;
  }
}

const BillpocketPlugin = new BillpocketPluginWeb();

export { BillpocketPlugin };

import { registerWebPlugin } from '@capacitor/core';
registerWebPlugin(BillpocketPlugin);
