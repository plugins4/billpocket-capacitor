declare module "@capacitor/core" {
  interface PluginRegistry {
    BillpocketPlugin: BillpocketPluginPlugin;
  }
}

export interface BillpocketPluginPlugin {
  echo(options: { value: string }): Promise<{value: string}>;
}
